export class UIContext {
  constructor() {}

  /**
   *
   * @param {string} elementId
   * @param {MouseEvent} eventHandler
   */
  mountClickEventToElementById(elementId, eventHandler) {
    const element = document.getElementById(elementId);
    element.onclick = eventHandler;
  }

  /**
   *
   * @param {string} elementId
   * @param {KeyboardEvent} eventHandler
   */
  mountOnInputEventToElementById(elementId, eventHandler) {
    const element = document.getElementById(elementId);
    element.oninput = eventHandler;
  }
}
