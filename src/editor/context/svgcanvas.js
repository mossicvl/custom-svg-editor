import { SVG } from '@svgdotjs/svg.js';
import '@svgdotjs/svg.draggable.js';
import '@svgdotjs/svg.panzoom.js';
import {
  MouseDownEvent,
  MouseMoveEvent,
  MouseUpEvent,
} from '../events/svgcanvas';

export class SvgCanvasContext {
  constructor() {
    const cWidth = '100%',
      cHeight = '100%';

    this.svgcanvas = SVG().addTo('#svgcanvas-container').size(cWidth, cHeight);
    this.svgcanvas.node.id = 'svgcanvas';
    this.svgcanvas.viewbox(
      0,
      0,
      this.svgcanvas.node.scrollWidth,
      this.svgcanvas.node.scrollHeight
    );

    this.onMouseUp = new MouseUpEvent();
    this.onMouseMove = new MouseMoveEvent();
    this.onMouseDown = new MouseDownEvent();

    this.svgcanvas.node.onmousedown = (e) => {
      this.onMouseDown.fire(e);
    };
    this.svgcanvas.node.onmousemove = (e) => {
      this.onMouseMove.fire(e);
    };
    this.svgcanvas.node.onmouseup = (e) => {
      this.onMouseUp.fire(e);
    };
    this.svgcanvas.node.onclick = (e) => {
      console.log(
        this.svgcanvas.children()[1].children()[0].inside(e.offsetX, e.offsetY)
      );
    };
  }

  createLayer() {
    return this.svgcanvas.group();
  }

  enablePanZoom(zoomFactor = 0.2) {
    this.svgcanvas.panZoom({ zoomFactor });
  }

  disablePanZoom() {
    this.svgcanvas.panZoom(false);
  }
}
