import { LayersContext } from './layers';
import { Point, Rect, Ellipse, Line, PointArray } from '@svgdotjs/svg.js';
import '@svgdotjs/svg.draggable.js';
import '../svgjs-extensions/selection';
import { SelectionContext } from './selection';

export class DrawingContext {
  /**
   *
   * @param {LayersContext} layersContext
   * @param {SelectionContext} selectionContext
   */
  constructor(layersContext, selectionContext) {
    /**
     * @private
     */
    this._layersContext = layersContext;
    /**
     * @private
     */
    this._selectionContext = selectionContext;
  }

  /**
   *
   * @param {Point} positionPoint
   * @param {number} width
   * @param {number} height
   * @returns {Rect}
   */
  drawSquare(positionPoint, width, height) {
    const {
      elementSelectedEvent,
      elementUnselectedEvent,
    } = this._selectionContext;

    return this._layersContext
      .getCurrentLayer()
      .rect(width, height)
      .move(positionPoint.x, positionPoint.y)
      .draggable()
      .selection({
        selectable: true,
        elementSelectedEvent,
        elementUnselectedEvent,
      });
  }

  /**
   *
   * @param {Point} positionPoint
   * @param {number} width
   * @param {number} height
   * @returns {Rect}
   */
  drawImage(positionPoint, width, height, src) {
    const {
      elementSelectedEvent,
      elementUnselectedEvent,
    } = this._selectionContext;

    return this._layersContext
      .getCurrentLayer()
      .image(src)
      .size(width === 0 ? 1 : width, height === 0 ? 1 : height)
      .move(positionPoint.x, positionPoint.y)
      .draggable()
      .selection({
        selectable: true,
        elementSelectedEvent,
        elementUnselectedEvent,
      });
  }

  /**
   *
   * @param {Point} positionPoint
   * @param {number} width
   * @param {number} height
   * @returns {Ellipse}
   */
  drawEllipse(positionPoint, width, height) {
    const {
      elementSelectedEvent,
      elementUnselectedEvent,
    } = this._selectionContext;

    return this._layersContext
      .getCurrentLayer()
      .ellipse(width, height)
      .move(positionPoint.x, positionPoint.y)
      .draggable()
      .selection({
        selectable: true,
        elementSelectedEvent,
        elementUnselectedEvent,
      });
  }

  /**
   *
   * @param {Point} point1
   * @param {Point} point2
   * @returns {Line}
   */
  drawLine(point1, point2) {
    const {
      elementSelectedEvent,
      elementUnselectedEvent,
    } = this._selectionContext;

    return this._layersContext
      .getCurrentLayer()
      .line(
        new PointArray([
          [point1.x, point1.y],
          [point2.x, point2.y],
        ])
      )
      .stroke({ width: 5, color: 'black' })
      .draggable()
      .selection({
        selectable: true,
        elementSelectedEvent,
        elementUnselectedEvent,
      });
  }
}
