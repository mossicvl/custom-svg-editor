export const TOOLS = {
  SELECTION: 'selection',
  LINE: 'line',
  ELLIPSE: 'ellipse',
  RECTANGLE: 'rectangle',
  IMAGE: 'image',
  ZOOM: 'zoom',
  BUILD_L_SYSTEM_RESULT: 'build_l_system_result',
};
