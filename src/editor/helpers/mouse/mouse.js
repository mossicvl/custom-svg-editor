import { Point } from '@svgdotjs/svg.js';

/**
 *
 * @param {Point} startPoint
 * @param {Point} endPoint
 */
export function getPositionPointByStartEndPoints(startPoint, endPoint) {
  if (startPoint.x < endPoint.x && startPoint.y < endPoint.y) return startPoint;
  else if (startPoint.x < endPoint.x && startPoint.y > endPoint.y)
    return new Point(startPoint.x, endPoint.y);
  else if (startPoint.x > endPoint.x && startPoint.y < endPoint.y)
    return new Point(endPoint.x, startPoint.y);
  else return endPoint;
}

/**
 *
 * @typedef {Object} ShapeAttributes
 * @property {number} x - The X Coordinate
 * @property {number} y - The Y Coordinate
 *
 * @param {Point} mouseTapStartPoint
 * @param {Point} mouseTapEndPoint
 * @returns {ShapeAttributes} Shape attributes
 */
export function generateShapeAttributes(mouseTapStartPoint, mouseTapEndPoint) {
  return {
    positionPoint: getPositionPointByStartEndPoints(
      mouseTapStartPoint,
      mouseTapEndPoint
    ),
    width: Math.abs(mouseTapStartPoint.x - mouseTapEndPoint.x),
    height: Math.abs(mouseTapStartPoint.y - mouseTapEndPoint.y),
  };
}
