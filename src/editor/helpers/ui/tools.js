import { TOOLS } from '../../constants/tools';

export function updateUiOnToolChanged(targetTool) {
  const toolElem = document.getElementById(getToolIdByToolType(targetTool));
  // unselect previously selected tool
  const toolsList = document.getElementById('tools-list');
  for (let i = 0; i < toolsList.children.length; i++) {
    const child = toolsList.children[i];
    if (child.classList.contains('current-tool')) {
      child.classList.remove('current-tool');
    }
  }

  toolElem.classList.add('current-tool');
}

export function getToolIdByToolType(tool) {
  switch (tool) {
    case TOOLS.SELECTION:
      return 'selection-tool-button';
    case TOOLS.LINE:
      return 'line-tool-button';
    case TOOLS.ELLIPSE:
      return 'ellipse-tool-button';
    case TOOLS.RECTANGLE:
      return 'rect-tool-button';
    case TOOLS.IMAGE:
      return 'image-tool-button';
    case TOOLS.ZOOM:
      return 'zoom-tool-button';

    default:
      break;
  }
}
