export function updateLayersList(layersNames, currentLayerName, onSelectLayer) {
  const layers = document.getElementById('layers-list');

  while (layers.firstChild) {
    layers.firstChild.remove();
  }

  layersNames.forEach((layerName) => {
    layers.appendChild(
      createLayersListItem(layerName, currentLayerName, onSelectLayer)
    );
  });
}

export function createLayersListItem(
  layerName,
  currentLayerName,
  onSelectLayer
) {
  const layerNameContainerNode = document.createElement('div');
  layerNameContainerNode.className = 'layer-name';
  if (layerName === currentLayerName) {
    layerNameContainerNode.classList.add('bold');
  }
  layerNameContainerNode.onclick = () => {
    onSelectLayer(layerName);
  };
  layerNameContainerNode.innerHTML = layerName;

  return layerNameContainerNode;
}
