import { Point } from '@svgdotjs/svg.js';
import { TOOLS } from '../../constants/tools';
import { generateShapeAttributes } from '../../helpers/mouse/mouse';

/**
 * @param {import('../initContexts').Contexts} contexts
 */
export function configureMouseContextSubscriptions({
  svgcanvasContext,
  mouseContext,
  toolsContext,
  drawingContext,
}) {
  svgcanvasContext.onMouseDown.addHandler((e) => {
    mouseContext.setIsMouseDown(true);
    // save mouse down point
    mouseContext.setMouseTapStartPoint(new Point(e.offsetX, e.offsetY));
  });

  svgcanvasContext.onMouseUp.addHandler((e) => {
    mouseContext.setIsMouseDown(false);
    // save mouse up point
    mouseContext.setMouseTapEndPoint(new Point(e.offsetX, e.offsetY));

    if (toolsContext.isCurrentToolForDrawing()) {
      const currentTool = toolsContext.getCurrentTool();
      // generate shape position point and shape sizes by start of drawing point and end of drawing point
      const { positionPoint, width, height } = generateShapeAttributes(
        mouseContext.getMouseTapStartPoint(),
        mouseContext.getMouseTapEndPoint()
      );

      switch (currentTool) {
        case TOOLS.LINE:
          drawingContext.drawLine(
            mouseContext.getMouseTapStartPoint(),
            mouseContext.getMouseTapEndPoint()
          );
          break;
        case TOOLS.ELLIPSE:
          drawingContext.drawEllipse(positionPoint, width, height);
          break;
        case TOOLS.RECTANGLE:
          drawingContext.drawSquare(positionPoint, width, height);
          break;
        case TOOLS.IMAGE:
          const imageSource = prompt(
            'Enter image source url: ',
            'https://pbs.twimg.com/profile_images/980681269859241984/-4cD6ouV_400x400.jpg'
          );
          drawingContext.drawImage(positionPoint, width, height, imageSource);
          break;

        default:
          break;
      }
    }

    mouseContext.setMouseTapStartPoint(null);
    mouseContext.setMouseTapEndPoint(null);
  });
}
