import { TOOLS } from '../../constants/tools';

/**
 * @param {import('../initContexts').Contexts} contexts
 */
export function configureSvgcanvasContextSubscriptions({
  svgcanvasContext,
  toolsContext,
}) {
  // subscribe on tool change event, to disable dragging elements, if we tries to draw(on them)
  toolsContext.toolChangedEvent.addHandler(({ tool }) => {
    if (tool === TOOLS.ZOOM) {
      svgcanvasContext.enablePanZoom();
    } else {
      svgcanvasContext.disablePanZoom();
    }
  });
}
