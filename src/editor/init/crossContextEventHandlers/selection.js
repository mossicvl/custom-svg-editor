/**
 * @param {import('../initContexts').Contexts} contexts
 */
//TODO: encapsulate into selection context
export function configureSelectionContextSubscriptions({ selectionContext }) {
  selectionContext.elementSelectedEvent.addHandler((args) => {
    selectionContext.addElementToSelection(args.element);
  });

  selectionContext.elementUnselectedEvent.addHandler((args) => {
    selectionContext.removeElementFromSelection(args.element);
  });
}
