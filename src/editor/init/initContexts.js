import '@svgdotjs/svg.draggable.js';
import { SvgCanvasContext } from '../context/svgcanvas';
import { LayersContext } from '../context/layers';
import { UIContext } from '../context/ui';
import { ToolsContext } from '../context/tools';
import { MouseContext } from '../context/mouse';
import { DrawingContext } from '../context/drawing';
import { Point } from '@svgdotjs/svg.js';
import { layersContextPreconfigurations } from './layers';
import { toolsContextPreconfigurations } from './tools';
import { uiContextPreconfigurations } from './ui';
import { configureLayersContextSubscriptions } from './crossContextEventHandlers/layers';
import { configureMouseContextSubscriptions } from './crossContextEventHandlers/mouse';
import { configureUIContextSubscriptions } from './crossContextEventHandlers/ui';
import { configureSvgcanvasContextSubscriptions } from './crossContextEventHandlers/svgcanvas';
import { SelectionContext } from '../context/selection';
import { configureSelectionContextSubscriptions } from './crossContextEventHandlers/selection';

export function initContexts() {
  const svgcanvasContext = new SvgCanvasContext();
  const toolsContext = new ToolsContext();
  const selectionContext = new SelectionContext();
  const layersContext = new LayersContext(svgcanvasContext, selectionContext);
  const drawingContext = new DrawingContext(layersContext, selectionContext);
  const mouseContext = new MouseContext(
    svgcanvasContext,
    toolsContext,
    drawingContext
  );
  const uiContext = new UIContext();

  initialAppSetup({
    svgcanvasContext,
    toolsContext,
    layersContext,
    drawingContext,
    mouseContext,
    uiContext,
    selectionContext,
  });
}

/**
 * @typedef {Object} Contexts
 * @property {SvgCanvasContext} contexts.svgcanvasContext
 * @property {LayersContext} contexts.layersContext
 * @property {SelectionContext} contexts.selectionContext
 * @property {DrawingContext} contexts.drawingContext
 * @property {ToolsContext} contexts.toolsContext
 * @property {MouseContext} contexts.mouseContext
 * @property {UIContext} contexts.uiContext
 *
 * @param {Contexts} contexts
 */
function initialAppSetup(contexts) {
  // TODO: check there is no order problems with this approach
  // cross context event listeners subscription
  configureLayersContextSubscriptions(contexts);
  configureMouseContextSubscriptions(contexts);
  configureUIContextSubscriptions(contexts);
  configureSvgcanvasContextSubscriptions(contexts);

  // contexts own pre-configurations.
  layersContextPreconfigurations(contexts);
  uiContextPreconfigurations(contexts);
  toolsContextPreconfigurations(contexts);
  configureSelectionContextSubscriptions(contexts);

  // TODO: remove after app will be finished
  // prepareSerpinskiTriangleOfCirclesExample(contexts);
  prepareSerpinskiTriangleOfImagesExample(contexts);
}

/**
 *
 * @param {Contexts} contexts
 */
function prepareSerpinskiTriangleOfCirclesExample(contexts) {
  const axiomLayer = contexts.layersContext.createLayer('axiom');
  contexts.drawingContext
    .drawEllipse(new Point(100, 100), 80, 80)
    .addClass('A');

  const rulesContext = contexts.layersContext.createLayer('rules');

  rulesContext
    .group()
    .addClass('forA')
    .add(
      contexts.drawingContext
        .drawEllipse(new Point(280, 280), 80, 80)
        .addClass('A')
        .draggable(false)
    )
    .add(
      contexts.drawingContext
        .drawEllipse(new Point(320, 200), 80, 80)
        .addClass('A')
        .draggable(false)
    )
    .add(
      contexts.drawingContext
        .drawEllipse(new Point(360, 280), 80, 80)
        .addClass('A')
        .draggable(false)
    )
    .draggable();
  contexts.layersContext.removeLayer('Layer0');
}

/**
 *
 * @param {Contexts} contexts
 */
function prepareSerpinskiTriangleOfImagesExample(contexts) {
  const axiomLayer = contexts.layersContext.createLayer('axiom');
  contexts.drawingContext
    .drawImage(
      new Point(100, 100),
      80,
      80,
      'https://pbs.twimg.com/profile_images/980681269859241984/-4cD6ouV_400x400.jpg'
    )
    .addClass('A');

  const rulesContext = contexts.layersContext.createLayer('rules');

  rulesContext
    .group()
    .addClass('forA')
    .add(
      contexts.drawingContext
        .drawImage(
          new Point(280, 280),
          80,
          80,
          'https://pbs.twimg.com/profile_images/980681269859241984/-4cD6ouV_400x400.jpg'
        )
        .addClass('A')
        .draggable(false)
    )
    .add(
      contexts.drawingContext
        .drawImage(
          new Point(320, 200),
          80,
          80,
          'https://pbs.twimg.com/profile_images/980681269859241984/-4cD6ouV_400x400.jpg'
        )
        .addClass('A')
        .draggable(false)
    )
    .add(
      contexts.drawingContext
        .drawImage(
          new Point(360, 280),
          80,
          80,
          'https://pbs.twimg.com/profile_images/980681269859241984/-4cD6ouV_400x400.jpg'
        )
        .addClass('A')
        .draggable(false)
    )
    .draggable();
  contexts.layersContext.removeLayer('Layer0');
}
